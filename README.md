# EfficiencyPlotter

Plots the B-Tagging Efficiencies when root filenames are supplied that are a result of running athena jobs in these two packages
https://gitlab.cern.ch/sgoswami/ftag_combinedtracks/ & https://gitlab.cern.ch/sgoswami/ftag_regulartracks/

run
```
root -l -b -q myfunc.C

```
